# Cerber #

Protect Zend Framework 2 based application from unauthorized access.
Released under MIT license.

## Installation

1. Add the bitbucket repository to `composer.json`:

        "repositories": [
          { "type": "vcs", "url": "https://michalz_plocman@bitbucket.org/michalz_plocman/cerber.git" }
        ]

2. Require the library: `composer require michalz_plocman/cerber`

3. Copy the config file to your config/autoload directory:

        cp vendor/michalz_plocman/cerber/config/cerber.local.php.dist config/autoload/cerber.local.php

4. Configure your `config/autoload/cerber.local.php` file with controller names
from the Controller Manager you wish to protect, your home route name and login route name.

