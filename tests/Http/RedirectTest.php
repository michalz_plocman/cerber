<?php
namespace CerberTest\Http;

use Cerber\Http\Redirect;

class RedirectTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_constructs()
    {
        $redirect = new Redirect('url');

        $this->assertEquals(['Location' => 'url'], $redirect->getHeaders()->toArray());
        $this->assertEquals(302, $redirect->getStatusCode());
    }
}
