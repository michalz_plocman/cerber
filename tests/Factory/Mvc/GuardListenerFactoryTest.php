<?php
namespace CerberTest\Factory\Mvc;

use App\Common\MailboxInterface;
use Cerber\Factory\Mvc\GuardListenerFactory;
use Cerber\Mvc\GuardListener;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Mvc\Router\RouteStackInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GuardListenerFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateService()
    {
        $router = $this->getMockBuilder(RouteStackInterface::class)
            ->getMockForAbstractClass();

        $authService = $this->getMockBuilder('stdClass')
            ->setMethods(['hasIdentity'])
            ->getMockForAbstractClass();

        $authService->expects($this->once())
            ->method('hasIdentity')
            ->will($this->returnValue(true));

        /** @var ServiceLocatorInterface|\PHPUnit_Framework_MockObject_MockObject $sm */
        $sm = $this->getMockBuilder(ServiceLocatorInterface::class)
            ->setMethods(['get'])
            ->getMockForAbstractClass();

        $sm->expects($this->at(0))
            ->method('get')
            ->with($this->equalTo('HttpRouter'))
            ->will($this->returnValue($router));

        $sm->expects($this->at(1))
            ->method('get')
            ->with($this->equalTo('config'))
            ->will($this->returnValue(['cerber' => [
                'dmz' => [],
                'guestOnly' => [],
                'loginRoute' => '',
                'homeRoute' => ''
            ]]));

        $sm->expects($this->at(2))
            ->method('get')
            ->with($this->equalTo('Zend\Authentication\AuthenticationService'))
            ->will($this->returnValue($authService));

        $factory = new GuardListenerFactory();

        $this->assertInstanceOf(GuardListener::class, $factory->createService($sm));
    }
}
