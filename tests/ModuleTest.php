<?php
namespace CerberTest;

use Cerber\Module;

class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /** @var Module */
    private $module;

    public function setUp()
    {
        $this->module = new Module();
    }

    public function testGetConfig()
    {
        $this->assertInternalType('array', $this->module->getConfig());
    }

    public function testGetServiceConfig()
    {
        $this->assertInternalType('array', $this->module->getServiceConfig());
    }
}
