<?php
namespace CerberTest\Mvc;

use Cerber\Http\Redirect;
use Cerber\Mvc\GuardListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface;

class GuardListenerTest extends \PHPUnit_Framework_TestCase
{
    /** @var MvcEvent|\PHPUnit_Framework_MockObject_MockObject */
    private $event;

    /** @var RouteMatch|\PHPUnit_Framework_MockObject_MockObject */
    private $routeMatch;

    /** @var RouteStackInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $router;

    public function setUp()
    {
        $this->router = $this->getMockBuilder(RouteStackInterface::class)
            ->setMethods(['assemble'])
            ->getMockForAbstractClass();

        $this->routeMatch = $this->getMockBuilder(RouteMatch::class)
            ->disableOriginalConstructor()
            ->setMethods(['getParam'])
            ->getMock();

        $this->routeMatch->expects($this->atLeastOnce())
            ->method('getParam')
            ->will($this->returnValue('controller'));

        $this->event = $this->getMockBuilder(MvcEvent::class)
            ->disableOriginalConstructor()
            ->setMethods(['stopPropagation', 'getRouteMatch'])
            ->getMock();

        $this->event->expects($this->any())
            ->method('getRouteMatch')
            ->will($this->returnValue($this->routeMatch));
    }

    public function test_it_allows_everybody_to_dmz_zones()
    {
        $listener = new GuardListener(['controller'], [], 'loginRoute', 'homeRoute', true, $this->router);

        $this->assertNull($listener->onDispatch($this->event));
    }

    public function test_it_allows_guests_to_guest_zones()
    {
        $listener = new GuardListener(['dmz'], ['controller'], 'loginRoute', 'homeRoute', false, $this->router);

        $this->assertNull($listener->onDispatch($this->event));
    }

    public function test_it_redirects_guests_to_login()
    {
        $this->event->expects($this->once())
            ->method('stopPropagation');

        $this->router->expects($this->once())
            ->method('assemble')
            ->with($this->equalTo([]), $this->equalTo(['name' => 'loginRoute']))
            ->will($this->returnValue('url'));

        $listener = new GuardListener(['dmz'], [], 'loginRoute', 'homeRoute', false, $this->router);

        $this->assertInstanceOf(Redirect::class, $listener->onDispatch($this->event));
    }

    public function test_it_disallows_logged_users_to_access_guestOnly_zones()
    {
        $this->event->expects($this->once())
            ->method('stopPropagation');

        $this->router->expects($this->once())
            ->method('assemble')
            ->with($this->equalTo([]), $this->equalTo(['name' => 'homeRoute']))
            ->will($this->returnValue('url'));

        $listener = new GuardListener(['dmz'], ['controller'], 'loginRoute', 'homeRoute', true, $this->router);

        $this->assertInstanceOf(Redirect::class, $listener->onDispatch($this->event));
    }

    public function test_it_allows_logged_in_users()
    {
        $this->event->expects($this->never())
            ->method('stopPropagation');

        $listener = new GuardListener(['dmz'], ['guest'], 'loginRoute', 'homeRoute', true, $this->router);

        $this->assertNull($listener->onDispatch($this->event));
    }
}
