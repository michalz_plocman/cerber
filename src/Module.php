<?php
namespace Cerber;

use Cerber\Factory\Mvc\GuardListenerFactory;
use Cerber\Mvc\GuardListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        if (PHP_SAPI === 'cli') {
            // do not run when in CLI mode
            // only applicable in HTTP context
            return;
        }
    
        $app = $e->getApplication();
        $evm = $app->getEventManager();
        $svm = $app->getServiceManager();

        if ($svm->has('Zend\Authentication\AuthenticationService')) {
            /** @var GuardListener $listener */
            $listener = $svm->get(GuardListener::class);
            $listener->attach($evm);
        }
    }

    public function getConfig()
    {
        return [
            'cerber' => [
                'dmz'       => [],
                'guestOnly' => [],
                'loginUrl'  => '/login',
                'homeUrl'   => '/',
            ],
        ];
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                GuardListener::class => GuardListenerFactory::class,
            ],
        ];
    }
}
