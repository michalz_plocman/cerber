<?php
namespace Cerber\Mvc;

use Cerber\Http\Redirect;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteStackInterface as Router;

class GuardListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /** @var array */
    protected $dmz;
    
    /** @var array */
    protected $guestOnly;

    /** @var string */
    protected $loginRoute;
    
    /** @var string */
    protected $homeRoute;
    
    /** @var bool */
    protected $hasIdentity;

    /** @var Router */
    protected $router;

    public function __construct(array $dmz, array $guestOnly, $loginRoute, $homeRoute, $hasIdentity, Router $router)
    {
        $this->dmz         = $dmz;
        $this->guestOnly   = $guestOnly;
        $this->loginRoute  = $loginRoute;
        $this->homeRoute   = $homeRoute;
        $this->hasIdentity = $hasIdentity;
        $this->router      = $router;
    }

    public function onDispatch(MvcEvent $e)
    {
        $ctrl = $e->getRouteMatch()->getParam('controller');

        if (in_array($ctrl, $this->dmz)) {
            // running controller is in the DMZ zone
            // allow entry to everybody
            return null;
        }

        if (! $this->hasIdentity) {
            // user not logged

            if (in_array($ctrl, $this->guestOnly)) {
                // controller is allowed for guest access
                return null;
            }

            // controller is not allowed for guests
            // redirect to login page
            $e->stopPropagation();
            return new Redirect($this->router->assemble([], ['name' => $this->loginRoute]));
        }

        // user logged
        // if trying to access the guestOnly controller, redirect to the home page
        if (in_array($ctrl, $this->guestOnly)) {
            // nie pozwól mu przebywać w strefie dla gości
            $e->stopPropagation();
            return new Redirect($this->router->assemble([], ['name' => $this->homeRoute]));
        }
        
        // at this point everything is fine
        // user should not be disallowed to enter

        return null;
    }

    public function attach(EventManagerInterface $events)
    {
        $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 1500);
    }
}
