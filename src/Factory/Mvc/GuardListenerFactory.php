<?php
namespace Cerber\Factory\Mvc;

use Cerber\Mvc\GuardListener;
use Zend\Mvc\Router\RouteStackInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GuardListenerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return GuardListener
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var RouteStackInterface $router */
        $router = $serviceLocator->get('HttpRouter');

        $config = $serviceLocator->get('config')['cerber'];

        return new GuardListener(
            $config['dmz'],
            $config['guestOnly'],
            $config['loginRoute'],
            $config['homeRoute'],
            $serviceLocator->get('Zend\Authentication\AuthenticationService')->hasIdentity(),
            $router
        );
    }
}
