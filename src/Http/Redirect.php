<?php
namespace Cerber\Http;

use Zend\Http\Response;

class Redirect extends Response
{
    public function __construct($url, $statusCode = 302)
    {
        $this->getHeaders()->addHeaderLine('Location', $url);
        $this->setStatusCode($statusCode);
    }
}
